<html>
  <head>
    <script type="text/javascript" language="javascript">
      var count = 0;
      function agregarVariable() {
        var tabla = document.getElementById("tabla-params");
        count++;
        tabla.innerHTML += "<tr id='variable-"+count+"'>"+
            "<td>Nombre:<input type='text' name='paramName[]'></td>"+
            "<td>Valor:<input type='text' name='paramValue[]'></td>"+
            "<td><input type='button' name='x' value='Quitar' onclick='quitarVariable("+ count +");'></td>"+
        "</tr>";
      }
      function quitarVariable(idCount) {
        document.getElementById('variable-'+idCount).remove();
      }
    </script>
  </head>
  <body><center>
    <h2>Iniciar Proceso</h2>
    <form id="form" action="start-process.php" method="post">
<?php
  require_once('connect.php');
  if (isset($_POST["processName"]) && $_POST["processName"] !== "") {
      $nameArray = $_POST["paramName"];
      $valueArray = $_POST["paramValue"];
      $params = array();
      for ($index = 0; $index < count($nameArray); $index++) {
        $key = "map_".$nameArray[$index];
        $value = $valueArray[$index];
        $params[$key] = utf8_encode($value);
      }
      $id = startProcess("org.jbpm:HR:1.0", $_POST["processName"], $params);
      echo "Proceso Iniciado con ID: $id<br>";
  }
?>
      <table id="tabla-params">
        <tr>
          <td>Nombre de Proceso</td>
          <td>
            <select name="processName">
              <option value=""></option>
              <option value="hiring">Proceso de contratacion</option>
            </select>
          </td>
          <td></td>
        </tr>
      </table>
      <input type="button" name="a" value="Agregar variable" onclick="agregarVariable();">
      &nbsp;<input type="submit" value="Empezar proceso">
    </form>
    <a href="/index.php">Volver</a>
  </center></body>
</html>
