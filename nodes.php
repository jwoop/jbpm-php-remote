<?php 
  require_once('connect.php');
  $id = $_REQUEST["processInstanceId"];
  if ($id == null) die ("No se puede mostrar esta pagina sin el ID de instancia de proceso<br><a href='get-processes.php'>Volver</a>");
  $nodes = getNodes($id);
?>
<html>
  <body><center>
    <h2>Historial de nodos de la instancia de proceso <?php echo $id;?></h2>
    <?php 
      if (count($variables) == 0) { 
        echo "<h5>No se encontro historial de nodos para la instancia de proceso $id</h5>" 
      } else { ?>
    <table id="tabla-params">
      <thead><th>Nombre de nodo</th><th>Tipo de nodo</th><th>Work Item</th><th>Fecha</th></thead>
      <?php
      date_default_timezone_set('America/Buenos_Aires');
      foreach ($nodes->historyLogList as $node) {
        $vars = get_object_vars($node);
        $instance = $vars["org.kie.services.client.serialization.jaxb.impl.audit.JaxbNodeInstanceLog"];
        if ($instance->type == 0) {
          $instance_date = date('d/m/Y H:i:s', ($instance->date/1000));
          echo "<tr><td>$instance->nodeName</td><td>$instance->nodeType</td><td>$instance->workItemId</td><td>$instance_date</td></tr>";
        }
      } ?>
    </table>
    <?php } ?>
    <a href="/get-processes.php">Volver</a>
  </center></body>
</html>
