# KIE Workbench: Acceso con la Remote API usando PHP #

Este es un ejemplo de c&oacute;mo se puede acceder remotamente el KIE Workbench desde no solamente otras aplicaciones Java, sino desde cualquier otro tipo de aplicaci&oacute;n. Este ejemplo utiliza scripts de PHP y la Remote API para poder acceder a un servidor del KIE Workbench, basandose en la instalación generada por el repositorio del libro **jBPM6 Developer Guide**. Pueden tomar el ejemplo clonando su repositorio y siguiendo los siguientes comandos:

```
git clone https://github.com/marianbuenosayres/jBPM6-Developer-Guide.git
cd jBPM6-Developer-Guide/chapter-04/kie-wb-installer
mvn clean install exec:exec
```

Una vez instalada, puede utilizarse estos scripts como estan mostrados para acceder al KIE Workbench instalado e iniciado por dichos comandos.

### Configuracion ###

Si se desea acceder a otra instalacion del KIE Workbench, simplemente deben editarse los comandos *define* del archivo connect.php:

```
define ('jbpm_host', 'localhost:8080');
define ('jbpm_rest_url', 'http://'.jbpm_host.'/kie-wb/rest');
define ('login_user', 'mariano');
define ('login_pass', 'mypass');
```