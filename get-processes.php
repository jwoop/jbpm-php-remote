<?php 
 function comoTexto($status) {
   $retval = "Desconocido";
   switch($status) {
     case 0: $retval = "Pendiente"; break;
     case 1: $retval = "Activo"; break;
     case 2: $retval = "Completado"; break;
     case 3: $retval = "Abortado"; break;
     case 4: $retval = "Suspendido"; break;
   }
   return $retval;
 }
?>
<html>
  <body><center>
    <h2>Ver instancias de proceso</h2>
    <form id="form" action="get-processes.php" method="post">
      <table id="tabla-params">
        <tr>
          <td>Nombre de Proceso</td>
          <td>
            <select name="processName">
              <option value=""></option>
              <option value="hiring">Proceso de contratacion</option>
            </select>
          </td>
          <td><input type="submit" name="a" value="Obtener instancias"></td>
        </tr>
      </table>
    </form>
    <?php
      require_once('connect.php');
      $processes = array();
      if (isset($_POST["processName"]) && $_POST["processName"] !== "") {
        $processes = getProcessInstances($_POST["processName"]);
      } else {
        $processes = getProcessInstances();
      }
      echo "<h3>Instancias de proceso</h3>";
      echo "<table><thead><th>ID</th><th>ID de Proceso</th><th>Estado</th><th>Comienzo</th><th>Acciones</th></thead>";
      date_default_timezone_set('America/Buenos_Aires');
      foreach ($processes->historyLogList as $process) {
        $vars = get_object_vars($process);
        $instance = $vars["org.kie.services.client.serialization.jaxb.impl.audit.JaxbProcessInstanceLog"];
        $instance_start = date('d/m/Y H:i:s', ($instance->start/1000));
        $instance_status = comoTexto($instance->status);
        echo "<tr><td>$instance->id</td><td>$instance->processId</td><td>$instance_status</td><td>$instance_start</td>";
        echo "<td><a href='variables.php?processInstanceId=$instance->id'>Variables</a>&nbsp;";
        echo "<a href='nodes.php?processInstanceId=$instance->id'>Historial</a></td></tr>";
      }
      echo "</table>";
    ?>
    <a href="/index.php">Volver</a>
  </center></body>
</html>
