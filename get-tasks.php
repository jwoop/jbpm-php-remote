<html>
  <body><center>
    <h2>Ver tareas de usuario</h2>
    <form id="form" action="get-tasks.php" method="post">
      <table id="tabla-params">
        <tr>
          <td>ID de Usuario</td>
          <td><input type="text" name="userId"></td>
          <td><input type="submit" name="a" value="Obtener tareas"></td>
        </tr>
      </table>
    </form>
    <?php
      require_once('connect.php');
      if (isset($_POST["userId"]) && $_POST["userId"] !== "") {
        $owned_tasks = getOwnedTasks($_POST["userId"]);
        $group_tasks = getGroupTasks($_POST["userId"]);
        $ownedIds = array();
        echo "<h3>Tareas propias</h3>";
        echo "<table><thead><th>ID</th><th>Nombre</th><th>Status</th><th>Prioridad</th></thead>";
        foreach ($owned_tasks->list as $task) {
          echo "<tr>";
          echo "<td>$task->id</td><td>$task->name</td><td>$task->status</td><td>$task->priority</td>";
          array_push($ownedIds, $task->id);
          echo "</tr>";
        }
        echo "</table>";
        echo "<h3>Tareas de grupo</h3>";
        echo "<table><thead><th>ID</th><th>Nombre</th><th>Status</th><th>Prioridad</th></thead>";
        foreach ($group_tasks->list as $task) {
          if(!(in_array($task->id, $ownedIds))) {
            echo "<tr><td>$task->id</td><td>$task->name</td><td>$task->status</td><td>$task->priority</td></tr>";
          }
        }
        echo "</table>";
      }
    ?>
    <a href="/index.php">Volver</a>
  </center></body>
</html>
