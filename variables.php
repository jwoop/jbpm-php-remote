<?php 
  require_once('connect.php');
  $id = $_REQUEST["processInstanceId"];
  if ($id == null) die ("No se puede mostrar esta pagina sin el ID de instancia de proceso<br><a href='get-processes.php'>Volver</a>");
  $variables = getVariables($id);
?>
<html>
  <body><center>
    <h2>Variables de la instancia de proceso <?php echo $id;?></h2>
    <?php 
      if (count($variables) == 0) { 
        echo "<h5>No se encontro historial de variables para la instancia de proceso $id</h5>" 
      } else { ?>
    <table id="tabla-params">
      <thead><th>Nombre</th><th>Valor</th></thead>
      <?php
      foreach ($variables->historyLogList as $var) {
        $vars = get_object_vars($var);
        $instance = $vars["org.kie.services.client.serialization.jaxb.impl.audit.JaxbVariableInstanceLog"];
        echo "<tr><td>$instance->variableId</td><td>$instance->value</td></tr>";
      } ?>
    </table>
    <?php } ?>
    <a href="/get-processes.php">Volver</a>
  </center></body>
</html>
