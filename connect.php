<?php

ini_set('show_errors', 1);
global $cookie;

define ('jbpm_host', 'localhost:8080');
define ('jbpm_rest_url', 'http://'.jbpm_host.'/kie-wb/rest');
define ('login_user', 'mariano');
define ('login_pass', 'mypass');

function getAuthorizedConnection($location, $method, $inputData = null) {
  $curl = curl_init($location);
  curl_setopt($curl, CURLOPT_VERBOSE, true);
  curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
  switch ($method) {
    case 'POST': curl_setopt($curl, CURLOPT_POST, true);
      if ($inputData != null) {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $inputData);
      }
      break;
    case 'PUT': curl_setopt($curl, CURLOPT_PUT, true);
      break;
  }
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_HEADER, 1);
  $httpheaders = array(
    "Accept: application/json",
    "Accept-Encoding: gzip, deflate",
    "Accept-Language: en-US,en;q=0.5",
    "Connection: keep-alive",
    "Content-Type: application/form-urlencoded;charset=UTF-8",
    "Host: ".jbpm_host,
    "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2",
    "WWW-Authenticate: Basic"
  );
  curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheaders);
  $url = str_replace("http://", "http://".login_user.":".login_pass."@", $location);
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($curl, CURLINFO_HEADER_OUT, 1);
  $outputData = curl_exec($curl);
  if ($outputData == false) {
    echo "Error de CURL: ".curl_errno($curl)."<br>";
  } else {
    $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $outputData = substr($outputData, $headerSize, strlen($outputData)); //remover headers
  }
  curl_close($curl);
  return $outputData;
}

function startProcess($deploymentId, $processId, $inputParams) {
  echo "INPUT PARAMS ES:<br>";
  var_dump($inputParams);
  echo "<br>";
  $location = jbpm_rest_url."/runtime/$deploymentId/withvars/process/$processId/start";
  $fullResult = getAuthorizedConnection($location, "POST", $inputParams);
  echo "Retorna de iniciar proceso: $processId<br>";
  return getInstanceIdFromResult($fullResult);
}

function completeWorkItem($deploymentId, $inputParams, $workItemId) {
  $location = jbpm_rest_url."/runtime/$deploymentId/workitem/$workItemId/complete";
  getAuthorizedConnection($location, "POST", $inputParams);
}

function getProcessInstanceState($deploymentId, $processInstanceId) {
  $location = jbpm_rest_url."/runtime/$deploymentId/process/instance/$processInstanceId";
  $fullResult = getAuthorizedConnection($location, "GET");
  json_decode($fullResult)[0]->status;
}

function getGroupTasks($userId) {
  $location = jbpm_rest_url."/task/query?potentialOwner=$userId";
  $fullResult = getAuthorizedConnection($location, "GET");
  return json_decode($fullResult);
}

function getOwnedTasks($userId) {
  $location = jbpm_rest_url."/task/query?taskOwner=$userId";
  $fullResult = getAuthorizedConnection($location, "GET");
  return json_decode($fullResult);
}

function getProcessInstances($processId = null) {
  $location = jbpm_rest_url."/history/instances";
  if ($processId != null) {
    $location = jbpm_rest_url."/history/process/$processId";
  }
  $fullResult = getAuthorizedConnection($location, "GET");
  return json_decode($fullResult);
}

function getVariables($processInstanceId) {
  $location = jbpm_rest_url."/history/instance/$processInstanceId/variable";
  $fullResult = getAuthorizedConnection($location, "GET");
  return json_decode($fullResult);
}

function getNodes($processInstanceId) {
  $location = jbpm_rest_url."/history/instance/$processInstanceId/node";
  $fullResult = getAuthorizedConnection($location, "GET");
  return json_decode($fullResult);
}

function getInstanceIdFromResult($fullResult) {
  $json = json_decode($fullResult);
  $retval = -1;
  if ($json != null) {
    $retval = $json->id;
  }
  return $retval;
}
?>
